#![feature(abi_x86_interrupt)]
#![no_main]
#![no_std]

use core::panic::PanicInfo;
use lazy_static::lazy_static;
use mini_os::{exit_qemu, QemuExitCode, serial_print, serial_println};
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};

lazy_static! {  // Needs special IDT to overwrite double fault handler with a custom one
    static ref TEST_IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();

        unsafe {
            idt.double_fault
                .set_handler_fn(test_double_fault_handler)
                .set_stack_index(mini_os::gdt::DOUBLE_FAULT_IST_INDEX);
        }

        idt
    };
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    serial_print!("stackoverflow::stack_overflow...\t");

    mini_os::gdt::init();
    init_test_idt();

    stack_overflow();

    panic!("Execuntion after stack overflow");
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    mini_os::test_panic_handler(info)
}

#[allow(unconditional_recursion)]
fn stack_overflow() {
    stack_overflow();
    volatile::Volatile::new(0).read();  // Prevent compiler from transforming recursion into loop
}

pub fn init_test_idt() {
    TEST_IDT.load();
}

extern "x86-interrupt"
fn test_double_fault_handler(_stack_frame: &mut InterruptStackFrame, _error_code: u64) -> ! {
    serial_println!("[ok]");
    exit_qemu(QemuExitCode::Success);
    loop {}
}
