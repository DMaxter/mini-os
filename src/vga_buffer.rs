use core::fmt;
use lazy_static::lazy_static;
use spin::Mutex;
use volatile::Volatile;

const BUFFER_ADDR: usize = 0xB8000;
// VGA text buffer has 25 rows and 80 columns
const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize  = 80;

const INVALID_CHAR: u8 = 0xDB;

// Global Writer
lazy_static! { // Initialize static runtime on first access
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer { // Safely synchronize interior mutability
        column: 0,
        color: ColorCode::new(Color::White, Color::Black),
        buffer: unsafe { &mut *(BUFFER_ADDR as *mut Buffer)},
    });
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga_buffer::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;

    interrupts::without_interrupts(|| { // Avoid deadlocks when printing (due to interrupts)
        WRITER.lock().write_fmt(args).unwrap();
    });
}

#[allow(dead_code)]  // Allow unused enum variants
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Color {
    Black      = 0,
    Blue       = 1,
    Green      = 2,
    Cyan       = 3,
    Red        = 4,
    Magenta    = 5,
    Brown      = 6,
    LightGray  = 7,
    DarkGray   = 8,
    LightBlue  = 9,
    LightGreen = 10,
    LightCyan  = 11,
    LightRed   = 12,
    Pink       = 13,
    Yellow     = 14,
    White      = 15
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]  // Ensure same memory layout as parameters
struct ColorCode(u8);

// Color byte has structure
// | BLINK (1 bit) | BACK (3 bits) | BRIGHT (1 bit) | FORE (3 bits) |

impl ColorCode {
    fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]  // Ensure correct field ordering
struct ScreenChar {
    ascii_char: u8,
    color_code: ColorCode,
}

#[repr(transparent)]
struct Buffer {
    chars: [[Volatile<ScreenChar>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

pub struct Writer {
    column:  usize,                // Last cursor position
    color:   ColorCode,
    buffer:  &'static mut Buffer,  // VGA text-buffer lives during whole runtime
}

impl Writer {
    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar {
            ascii_char: b' ',
            color_code: self.color,
        };

        for col in 0..BUFFER_WIDTH {
            self.buffer.chars[row][col].write(blank);
        }
    }

    fn newline(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let character = self.buffer.chars[row][col].read();
                self.buffer.chars[row - 1][col].write(character);
            }
        }

        self.clear_row(BUFFER_HEIGHT - 1);
        self.column = 0;
    }

    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.newline(),
            byte => {
                if self.column >= BUFFER_WIDTH {
                    self.newline();
                }

                let row = BUFFER_HEIGHT - 1;
                let col = self.column;
                let color = self.color;

                self.buffer.chars[row][col].write(ScreenChar {
                    ascii_char: byte,
                    color_code: color,
                });

                self.column += 1;
            }
        }
    }

    pub fn write(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                // Printable ASCII or newline
                0x20..=0x7E | b'\n' => self.write_byte(byte),
                // Print invalid char
                _ => self.write_byte(INVALID_CHAR),
            }
        }
    }
}

// Allow usage of the `write!` macro
impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write(s);
        Ok(())
    }
}


/// Print to the VGA buffer
#[test_case]
fn test_println_simple() {
    println!("Just testing the VGA buffer");
}

/// Print to the VGA buffer multiple times
#[test_case]
fn test_println_multiple() {
    for _ in 0..200 {
        println!("Testing again multiple times")
    }
}


/// Test output of println on screen
#[test_case]
fn test_println_output() {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;

    let s = "Test string to screen";
    interrupts::without_interrupts(|| {
        let mut writer = WRITER.lock();
        writeln!(writer, "\n{}", s).expect("writeln failed");

        for (i, c) in s.chars().enumerate() {
            let screen_char = writer.buffer.chars[BUFFER_HEIGHT-2][i].read();
            assert_eq!(char::from(screen_char.ascii_char), c);
        }
    })
}
