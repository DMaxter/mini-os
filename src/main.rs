#![no_std]   // No standard library linking
#![no_main]  // Not normal entry point
#![feature(custom_test_frameworks)]           // Custom test framework because cannot use std
#![test_runner(mini_os::test_runner)]
#![reexport_test_harness_main = "test_main"]  // Redefine main function for testing

use bootloader::{BootInfo, entry_point};
use core::panic::PanicInfo;
use mini_os::println;

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {  // Overwrite entry point function
    use mini_os::memory::{self, BootInfoFrameAllocator};
    use x86_64::{VirtAddr, structures::paging::Page};

    println!("Hello World!");
    // Initialize the system
    mini_os::init();

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe {
        BootInfoFrameAllocator::init(&boot_info.memory_map)
    };

    // Map an unused page
    let page = Page::containing_address(VirtAddr::new(0));
    memory::create_example_mapping(page, &mut mapper, &mut frame_allocator);

    // Write to screen using new mapping
    let page_ptr: *mut u64 = page.start_address().as_mut_ptr();
    unsafe { page_ptr.offset(400).write_volatile(0x_f021_f077_f065_f04e)};

    #[cfg(test)]
    test_main();

    mini_os::hlt_loop();
}

#[cfg(not(test))]
#[panic_handler]  // Replace the `panic!` macro
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);

    mini_os::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    mini_os::test_panic_handler(info);
}

